import React, { useEffect, useState } from 'react'
import { getCurrentUser } from '../../api/backend/account'
import Profile from '../../components/account/Profile'

function ProfileView(){

    const [profile, setProfile] = useState({})
    const [profileRetrieved, setProfileRetrieved] = useState(false)

    useEffect(() => {
        getCurrentUser()
        .then(res => {
            res.status === 200 && setProfile(res.data)
            setProfileRetrieved(true)
        })
    }, [])

    return(
        <>
          { profileRetrieved && <Profile currentProfile={profile} /> }
        </>
    )
}

export default ProfileView