import React from "react";
import { MDBContainer } from "mdbreact";
import PasswordRecoveryEmailCheck from './../../components/account/PasswordRecoveryEmailCheck';
import { requestPasswordReset } from './../../api/backend/account';
import { toast } from 'react-toastify';

/**
 * View of the page checking for email before password recovery
 * 
 * @returns the PasswordRecoveryEmailCheckView component
 * 
 * @author Cecile
 */
const PasswordRecoveryEmailCheckView = () => {

    const handleRequestPasswordReset = (values) => {

        requestPasswordReset(values).then(response => {
            toast.success("Email de récupération envoyé. Veuillez vérifier votre boîte de réception.")
        }).catch(() => {
            toast.error("Erreur de traitement de la demande.");
        })
    }

    return (
        <MDBContainer className="d-flex justify-content-center flex-column ">
            <PasswordRecoveryEmailCheck submit={handleRequestPasswordReset} />
        </MDBContainer>
    );
};

export default PasswordRecoveryEmailCheckView;