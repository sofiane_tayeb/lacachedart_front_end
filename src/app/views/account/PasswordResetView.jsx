import React, { useState } from "react";
import { MDBContainer } from "mdbreact";
import { finishPasswordReset } from './../../api/backend/account';
import { toast } from 'react-toastify';
import { URL_LOGIN } from './../../shared/constants/urls/urlConstants';
import PasswordReset from '../../components/account/PasswordReset';
import { useParams } from "react-router-dom";

/**
 * View of the page where the user can reset their password
 * 
 * @returns the PasswordResetView component
 * 
 * @author Cecile
 */
const PasswordResetView = ({ history }) => {

    const [errorForm, setErrorForm] = useState(false)
    const { key } = useParams();

    const handlePasswordReset = (values) => {

        let passwordReset = {
            key: key,
            newPassword: values.newPassword,
        }

        finishPasswordReset(passwordReset).then(response => {
            toast.success("Mot de passe réinitialisé.");
            history.push(URL_LOGIN);
        }).catch(() => {
            setErrorForm(true);
            toast.error("Erreur de traitement de la demande.");
        })
    }

        return (
            <MDBContainer className="d-flex justify-content-center flex-column ">
                <PasswordReset submitForm={handlePasswordReset} errorForm={errorForm} />
            </MDBContainer>
        );
    
};

export default PasswordResetView;