import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { authenticate } from './../api/backend/account';
import Login from '../components/account/Login';
import { isAuthenticated } from '../shared/services/accountServices';
import { URL_EMAIL_CHECK, URL_HOME } from '../shared/constants/urls/urlConstants';
import { signIn } from '../shared/redux-store/actions/authenticationActions';
import PasswordForgottenButton from '../components/account/PasswordForgottenButton';
import { MDBCard, MDBCardBody, MDBCardTitle } from 'mdbreact';
import { toast } from 'react-toastify';


const LoginView = ({ history }) => {

    const [errorLog, setErrorLog] = useState(false)
    const dispatch = useDispatch()


    useEffect(() => {
        const user = localStorage.getItem('token')

        if (isAuthenticated && user && user !== 'undefined') {
            toast.error("Vous êtes déjà connecté !")
            history.push(URL_HOME)
        }
    }, [history])


    const handleLogin = (values) => {

      authenticate(values).then(response => {

            if (response.status === 200 && response.data.id_token) {
                dispatch(signIn(response.data.id_token))

                if (isAuthenticated) {
                    toast.success("Identification réussie. Ravi de vous revoir !");
                    history.push(URL_HOME);
                }
            }
        }).catch((error) => {
            console.log(error)
            if (error.response.status === 400 || error.response.status === 401) {
                toast.error("Erreur de connexion.")
            } else if (error.response.status === 403) {
                toast.error("Erreur, ce compte n'est plus autorisé à se connecter.")
            }
        })
    }


    const handleToEmailCheck = () => {
        history.push(URL_EMAIL_CHECK)
    }

    return (

        <div className="container d-flex justify-content-center mt-5">

            <MDBCard style={{ width: "22rem" }}>
                <MDBCardBody className="ses-background-2">

                    <MDBCardTitle className="text-center">Connexion</MDBCardTitle>

                    <Login submit={handleLogin} errorLog={errorLog} />
                    <br />
                    <PasswordForgottenButton click={handleToEmailCheck} />

                </MDBCardBody>
            </MDBCard>

        </div>
    );
};

export default LoginView