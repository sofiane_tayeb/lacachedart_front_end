import React from 'react';
import CategoryChoose from '../../components/product/CategoryChoose';
import { useSelector } from 'react-redux';


const ProductCreateView = ({ history }) => {

    const isLogged = useSelector(({ authenticationReducer: { isLogged } }) => isLogged)


    return (
        <>
            { isLogged ?
                <CategoryChoose />
            : ""}
        </>
    )

}

export default ProductCreateView;