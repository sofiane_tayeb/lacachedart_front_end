import React, { useEffect, useState } from 'react'
import ProductsUser from '../../../components/dashboard/user/ProductsUser'
import Loader from '../../../shared/components/utils-components/Loader'
import { retrieveAllUserProducts } from '../../../api/backend/userProducts'

/**
 * @author jerome
 *  
 * Retrieves a pageable list of products belonging to current user
 * 
 */
function ProductsUserView() {

    const [userProducts, setUserProducts] = useState([])
    const [dataRetrieved, setDataRetrieved] = useState(false)


    useEffect(() => {
        retrieveAllUserProducts()
        .then(res => {
            res.status === 200 && setUserProducts(res.data)
            console.log(JSON.stringify(userProducts))
            setDataRetrieved(true)
        })
    }, [])

    return(
        <>
            {
                dataRetrieved && <ProductsUser userProductsRetrieved={userProducts} />
            }
        </>
    )
}

export default ProductsUserView