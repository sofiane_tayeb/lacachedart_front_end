import apiBackEnd from './api.Backend';
import { URL_BACK_UPDATE_ACCOUNT, URL_BACK_AUTHENTICATE, URL_BACK_UPDATE_PASSWORD, URL_BACK_REQUEST_PASSWORD_RESET, URL_BACK_FINISH_PASSWORD_REST, URL_BACK_REGISTER_ACCOUNT, URL_BACK_GET_ACCOUNT, URL_GET_ACCOUNT } from './../../shared/constants/urls/urlBackEnd';

export function authenticate(values) {
    return apiBackEnd.post(URL_BACK_AUTHENTICATE, values)
}

export function updatePassword(values){
    return apiBackEnd.post(URL_BACK_UPDATE_PASSWORD, values)
}

export function requestPasswordReset(values){
    return apiBackEnd.post(URL_BACK_REQUEST_PASSWORD_RESET, values.mail, {headers:{"Content-Type" : "application/json"}} )
}

export function finishPasswordReset(values) {
    return apiBackEnd.post(URL_BACK_FINISH_PASSWORD_REST, values)
}

export function geAccount() {
    return apiBackEnd.get(URL_GET_ACCOUNT)
}

/**
 * @author jerome
 * 
 * @param form values 
 * @returns a post axios with API as a constant and values given by user 
 */
export function registerUser(values){
    return apiBackEnd.post(URL_BACK_REGISTER_ACCOUNT, values)
}

export function getCurrentUser(){
    return apiBackEnd.get(URL_BACK_GET_ACCOUNT)
}

export function updateCurrentUser(values){
    return apiBackEnd.post(URL_BACK_UPDATE_ACCOUNT, values)
}