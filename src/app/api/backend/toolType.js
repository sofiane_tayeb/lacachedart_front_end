
import apiBackEnd from './api.Backend';
import { URL_BACK_GET_TOOLTYPE_BY_CATEGORY } from './../../shared/constants/urls/urlBackEnd';

export function getToolTypesByCategoryId(categoryId) {
    return apiBackEnd.get(`${URL_BACK_GET_TOOLTYPE_BY_CATEGORY}${categoryId}`)
}