import apiBackEnd from './api.Backend';
import {URL_BACK_BRAND} from './../../shared/constants/urls/urlBackEnd';


export function getAllBrands(){
    return apiBackEnd.get(URL_BACK_BRAND)
}