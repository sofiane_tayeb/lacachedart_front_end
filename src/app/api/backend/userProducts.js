import { URL_BACK_PRODUCTS_BY_USER } from '../../shared/constants/urls/urlBackEnd'
import apiBackEnd from './api.Backend'


function retrieveAllUserProducts(){
    return apiBackEnd.get(URL_BACK_PRODUCTS_BY_USER)
}

export {
    retrieveAllUserProducts
}
