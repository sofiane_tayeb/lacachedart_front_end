import apiBackEnd from './api.Backend';
import { URL_BACK_MAIN_CATEGORIES, URL_BACK_SUBCATEGORIES } from './../../shared/constants/urls/urlBackEnd';


export function getAllMainCategories() {
    return apiBackEnd.get(URL_BACK_MAIN_CATEGORIES)
}

export function getSubcategoriesByMainCategoryId(mainCategoryId) {
    return apiBackEnd.get(`${URL_BACK_SUBCATEGORIES}${mainCategoryId}`)
}