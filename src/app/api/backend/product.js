import apiBackEnd from './api.Backend';
import { URL_BACK_PRODUCT } from './../../shared/constants/urls/urlBackEnd';


export function createProduct(values) {
    return apiBackEnd.post(URL_BACK_PRODUCT, values)
}