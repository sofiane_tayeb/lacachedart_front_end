export const URL_BACK_AUTHENTICATE = '/authenticate'
export const URL_GET_ACCOUNT = '/account'
export const URL_BACK_UPDATE_PASSWORD = '/account/change-password'
export const URL_BACK_REGISTER_ACCOUNT = '/register'
export const URL_BACK_GET_ACCOUNT = '/account'
export const URL_BACK_UPDATE_ACCOUNT = '/account'

// back-end url for requesting a password change
export const URL_BACK_REQUEST_PASSWORD_RESET = '/account/reset-password/init'
export const URL_BACK_FINISH_PASSWORD_REST = '/account/reset-password/finish'

// APIs linked to Product
export const URL_BACK_PRODUCTS_BY_USER = '/products-belonging-to-user'
// back-end url concerning categories
export const URL_BACK_MAIN_CATEGORIES = '/main-categories'
export const URL_BACK_SUBCATEGORIES = '/subcategories/'

// back-end url concerning toolTypes
export const URL_BACK_GET_TOOLTYPE_BY_CATEGORY = '/tool-types-by-category/'

// back-end url concerning brands
export const URL_BACK_BRAND = '/brands'

// back-end url concerning products
export const URL_BACK_PRODUCT = '/products'
