export const URL_HOME = '/'
export const URL_LOGIN = '/login'
export const URL_ADMIN_HOME = '/admin'
export const URL_REGISTER = '/register'
export const URL_PROFILE = '/profile'

// front end url concerning password recovery
export const URL_EMAIL_CHECK = '/email-check'
export const URL_PASSWORD_RESET = '/password-reset/:key'
export const URL_PASSWORD_RESET_NO_KEY = '/password-reset/'

//url pour dashboard
export const URL_DASHBOARD = '/app'


//URIs linked to Product
export const URL_USER_DASHBOARD_PRODUCTS = '/dashboard/user/products'
export const URL_CHOOSE_CATEGORY = '/choose-category'
export const URL_CREATE_PRODUCT = '/create-product'
