import * as Yup from 'yup'

export const schemaFormLogin = Yup.object().shape({
    username: Yup.string().required("Champs requis"),
    password: Yup.string().required("Champs requis")
})

export const schemaFormUser = Yup.object().shape({
    idUserOtherInfo : Yup.string(),
    firstName : Yup.string().required("Champs requis"),
    lastName : Yup.string().required("Champs requis"),
    email : Yup.string().required("Champs requis"),
    phone :Yup.string().required("Champs requis"),
    address : Yup.object({
        id : Yup.string(),
        number : Yup.string().required("Champs requis"),
        streetAddress : Yup.string().required("Champs requis"),
        additional : Yup.string(),
        postalCode: Yup.string().required("Champs requis"),
        city : Yup.string().required("Champs requis"),
        country : Yup.string().required("Champs requis")
    })
})

export const schemaFormNewPassword = Yup.object().shape({
    currentPassword : Yup.string().required("Champs requis"),
    newPassword : Yup.string().required("Champs requis"),
    confirmPassword : Yup.string().required("Champs requis").oneOf([Yup.ref("newPassword"), null], "Les mots de passe ne sont pas similaires")

})

export const schemaFormEmailCheck = Yup.object().shape({
    mail : Yup.string().required("Champs requis")
})

export const schemaPasswordReset = Yup.object().shape({
    newPassword : Yup.string().required("Champs requis"),
    newPasswordRepeat : Yup.string().required("Champs requis").oneOf([Yup.ref("newPassword"), null], "Les mots de passe ne sont pas similaires")
})

export const schemaProductCreate = Yup.object().shape({
    name : Yup.string().required("Champs requis"),
    brandId : Yup.string().ensure().required("Veuillez choisir une marque"),
    categoryId : Yup.string().ensure().required("Veuillez choisir une pratique"),
    toolTypeId : Yup.string().ensure().required("Veuillez choisir un type d'outil"),
    description : Yup.string().required("Champs requis"),
    manufacturerPrice : Yup.number().typeError("Veuillez entrer un chiffre").positive("Veuillez entrer un chiffre positif")
})

export const schemaStoreCreate = Yup.object().shape({})