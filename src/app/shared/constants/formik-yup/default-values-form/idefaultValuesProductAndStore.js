export const initialValuesProductAndStoreCreate = {

    city: "",
    complement: "",
    country: "",
    gps: "",
    id: 0,
    name: "",
    postalCode: "",
    products: [
        {
            active: false,
            brandId: 0,
            categoryId: 0,
            creationDate: "",
            description: "",
            id: 0,
            manufacturerPrice: 0,
            name: "",
            reference: "",
            toolTypeId: 0,
            userOtherInfoId: 0
        }
    ],
    streetName: "",
    streetNumber: "",
    url: ""
}