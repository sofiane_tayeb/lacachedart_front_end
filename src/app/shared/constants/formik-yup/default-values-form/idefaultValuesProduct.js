export const initialValuesProductCreate = {
    active: false,
    brandId: 0,
    categoryId: 0,
    creationDate: new Date(),
    description: "",
    name: "",
    reference: "",
    toolTypeId: 0,
}