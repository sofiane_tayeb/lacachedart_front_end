export const defaulValuesLogin = { 
    username:'', 
    password:'' 
}

export const initialValuesUserOtherInf = (salesman) => ({
    idUserOtherInfo : salesman.idUserOtherInfo,
    firstName : salesman.firstName ,
    lastName : salesman.lastName,
    email : salesman.email,
    phone : salesman.phone,
    address : {
        id : salesman.address.id,
        number : salesman.address.number,
        streetAddress : salesman.address.streetAddress,
        additional : salesman.address.additional,
        postalCode: salesman.address.postalCode,
        city : salesman.address.city,
        country : salesman.address.country
    }
})

export const initialValuesNewPassword = {
    currentPassword : "",
    newPassword : "",
}

export const initialValuesEmailCheck = {
    mail : ""
}

export const initialValuesPasswordReset = {
    newPassword : "",
    newPasswordRepeat : ""
}