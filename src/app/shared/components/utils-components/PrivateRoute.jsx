import React from "react";
import {Redirect, Route} from "react-router-dom";
import { isAuthenticated, accountRoles } from '../../services/accountServices';
import { URL_HOME, URL_LOGIN } from '../../constants/urls/urlConstants';


export const PrivateRoute = ({component: Component, roles, companyAccess = false, ...rest}) => {
    
    return (
        <Route 
            {...rest}
            render={ props => {
                if (!isAuthenticated())
                    return <Redirect to={{ pathname: URL_LOGIN, state: {from: props.location} }}/>

                if(roles){
                    const rolesUser = accountRoles()    
                    if(!roles.some(r=> rolesUser.indexOf(r) >= 0))
                        return <Redirect to={{pathname: URL_HOME}}/>
                }

                return <Component {...props} />
            }}
        />
    )
};