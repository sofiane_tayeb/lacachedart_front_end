import React,  { useState, useEffect } from 'react';

const Loader = ({ small, big }) => {
    const [err, setErr] = useState(false);

    useEffect(() => {
        const interval = setInterval(() =>  setErr(true) , 15000);
        return () => clearInterval(interval);
    }, []);

  return (
    <>
    {!err ? (
        <div className="vh-center" >
            <div className="spinner-border text-danger" role="status">
                <span className="sr-only">Chargement...</span>
            </div>
        </div>
    ) : (
        <div className="alert alert-danger" role="alert">
            Erreur lors du chargement !
        </div>
    )}
    </>
  );
};

export default Loader;