import { useState, useEffect } from 'react'

/**
 * @author jerome
 * 
 * Gets dynamically window dimensions
 * 
 * @returns window dimensions
 */
function getWindowDimensions() {
    const { innerWidth: width, innerHeight: height } = window;
    return {
      width,
      height
    };
  }
  
  export default function useWindowDimensions() {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());
  
    useEffect(() => {
      function handleResize() {
        setWindowDimensions(getWindowDimensions());
      }
      
      //As soon as the listener finds a new event, set windowDimensions hook to new retrieved dimensions
      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
    }, []);
  
    return windowDimensions;
  }