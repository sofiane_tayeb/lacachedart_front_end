import React from 'react';
import { Formik, Form, Field } from 'formik';
import { MDBBtn } from 'mdbreact';
import { InsyMDBChoice, InsyMDBInput, InsyMDBSelect } from './../form-and-error-components/InputCustom';

const initialValues = {
    name: '',
    gender: '',
    hobby: ''
}

const genderOptions = [
    {label:'Female', value:'female'},
    {label:'Male', value:'male'},
    {label:'Other', value:'other'},
]

const optionsHobbies = [
    {text:'Tennis', value:'tennis'},
    {text:'Football', value:'football'},
    {text:'Karaté', value:'karate'},
    {text:'Natation', value:'natation'},
]
const FormExample = () => {

    const submit = values => console.log(values);

    return (
        <Formik initialValues={initialValues} onSubmit={submit}>
            {({values}) => (
                <Form>
                    <InsyMDBChoice
                        type='radio'
                        name='gender'
                        options={ genderOptions }
                        valueSelected={values.gender}
                        inline
                    />
                    <Field
                        type="text" 
                        name="name" 
                        placeholder="Name" 
                        component={ InsyMDBInput } 
                        errorRight
                    />
                    <Field
                        type="select"
                        name="hobby"
                        options={optionsHobbies}
                        component={InsyMDBSelect}
                        label="Hobby"
                    />
                    <MDBBtn 
                        type='submit' 
                        color="dark" 
                        className="btn-block"
                    >
                        Submit
                    </MDBBtn>
                </Form>
            )}
        </Formik>
    );
};

export default FormExample;