import React from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader } from "mdbreact";
import PropTypes from 'prop-types';

//TODO: to try it

/**
 * A component to create a simple modal
 * 
 * @param {boolean} isOpen: to show or not the modal REQUIRED
 * @param {function} toggle: the function to toggle or not the modal
 * @param {string/component} title: title displayed in the modal
 * @param {boolean} withBtn: to stipulate if the modal has a button
 * @param {function} btnClick: function use on the onClick
 * @param {boolean} disabledBtn: to disabled the button or not
 * @param {string/component} btnText: text display in the button
 * 
 * @author Mohamed Nechab
 */
const SimpleModal = ({ children, isOpen, toggle, title, withBtn, btnClick, disabledBtn, btnText }) => {
    console.log(children);
    return (
        <MDBModal isOpen={isOpen} toggle={toggle}>
            <MDBModalHeader className="text-dark">{title}</MDBModalHeader>
            <MDBModalBody>{children}</MDBModalBody>
            {withBtn &&
                <MDBModalFooter>
                    <MDBBtn color="primary" onClick={btnClick} disabled={disabledBtn}>{btnText}</MDBBtn>
                </MDBModalFooter>
            }
        </MDBModal>
    );
}

SimpleModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    toggle: PropTypes.func,
    title: PropTypes.string,
    btnClick: PropTypes.func,
    disabledBtn: PropTypes.bool,
    withBtn: PropTypes.bool
};

SimpleModal.defaultProps = {
    disabledBtn: false,
    withBtn: false
};

export default SimpleModal;