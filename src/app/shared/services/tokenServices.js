import jwt from 'jsonwebtoken'

const TOKEN_NAME = 'token'


export function setToken(token) {
    localStorage.setItem(TOKEN_NAME, token)
}


export function getToken() {
    return localStorage.getItem(TOKEN_NAME)
}


export function removeToken() {
    localStorage.removeItem(TOKEN_NAME)
}


export function getPayloadToken(){
    const decoded = decodeToken();
    return decoded.payload
}


function decodeToken() {
    const token = getToken()
    return jwt.decode(token, {complete: true});
}


 export function accountIdUser(){
    const payload = getPayloadToken()
    return payload.idUser
}


export function accountIdUserOtherInfo(){
    const payload = getPayloadToken()
    return payload.idUserOtherInfo
}