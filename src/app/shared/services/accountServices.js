import { getToken, getPayloadToken } from './tokenServices';


export function accountRoles() {
    const payload = getPayloadToken()
    return payload.auth.split(",")
}


export function accountLogin(){
    const payload = getPayloadToken()
    return payload.sub
}


export function isAuthenticated() {
    try{
        const token = getToken()
        const payload = getPayloadToken()
        const roles = payload.auth.split(",")
        const expirationDate = payload.exp
        const login = payload.sub
        const dateNow = new Date();
        return token
            && roles.length > 0
            && login
            && expirationDate < dateNow.getTime()
    } catch {
        return false
    }
}