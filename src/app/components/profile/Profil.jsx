import React, { useState }  from 'react'
import { MDBContainer, MDBCard, MDBCardTitle, MDBCardBody, MDBBtn } from 'mdbreact';
import AccountImg from '../../assets/images/account.svg'

import ProfilAndUpdate from './ProfilAndUpdate';
import UpdatePassword from './UpdatePassword';


const Profil = ({salesman, setSalesman}) => {

   const [updatePassword, setUpdatePassword] = useState(false)

   const render = (
        !updatePassword 
        ?   <>
                <ProfilAndUpdate salesman={salesman} setSalesman={setSalesman} /> 
                <MDBBtn onClick={() => setUpdatePassword(!updatePassword)}> Modifier Password </MDBBtn>
            </>
        : 
            <>
                <UpdatePassword setUpdatePassword={setUpdatePassword} />
                <MDBBtn onClick={() => setUpdatePassword(!updatePassword)}>Retour </MDBBtn>
            </>
   )


    return (

        <MDBContainer className="mt-5 ">
            <MDBCard>
                <img src={AccountImg} alt="account" style={{width: "100px", margin: "0 auto"}} className="text-center mt-3 mb-3"/>
                <MDBCardTitle className="text-center mt-2">Profil {salesman.login} </MDBCardTitle>
                <MDBCardBody>
                   {render}
                </MDBCardBody>
            </MDBCard>
        </MDBContainer>

    )
}

export default Profil;
