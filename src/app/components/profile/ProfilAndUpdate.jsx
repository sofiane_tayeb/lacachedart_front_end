import React, { useState }  from 'react'
import { MDBBtn } from 'mdbreact';
import { Formik, Form, Field } from 'formik';
import ErrorMessSmall from './../../shared/components/form-and-error-components/ErrorMessSmall';
import { InsyMDBInput } from '../../shared/components/form-and-error-components/InputCustom';

import { initialValuesUserOtherInf } from '../../shared/constants/formik-yup/default-values-form/idefaultValuesUser';
import { schemaFormUser } from '../../shared/constants/formik-yup/yup/yupUser';
import { updateSalesman } from '../../api/backend/salesman';

const FormProfil = ({ submitForm, errorForm, salesman, isSubmitting }) => (
    <Formik initialValues={initialValuesUserOtherInf(salesman)} onSubmit={submitForm} validationSchema={schemaFormUser}>
        <Form>
            <Field type="hidden" name="idUserOtherInfo" component={ InsyMDBInput } errorRight />
            <Field type="text" name="firstName" placeholder="firstname" component={ InsyMDBInput } errorRight/>
            <Field type='text' name='lastName' placeholder='lastName' component={ InsyMDBInput } errorRight />
            <Field type='text' name='email' placeholder='email' component={ InsyMDBInput } errorRight />
            <Field type='text' name='phone' placeholder='telephone' component={ InsyMDBInput } errorRight />
            <div>
                <Field type="hidden" name="address.id" component={ InsyMDBInput } errorRight />
                <Field type='text' name="address.number" placeholder='Numéro de voie' component={ InsyMDBInput } errorRight />
                <Field type='text' name="address.streetAddress" placeholder='adresse' component={ InsyMDBInput } errorRight />
                <Field type='text' name="address.additional" placeholder="complément d'adresse" component={ InsyMDBInput } errorRight />
                <Field type='text' name="address.postalCode" placeholder='Code Postal' component={ InsyMDBInput } errorRight />
                <Field type='text' name="address.city" placeholder='Ville' component={ InsyMDBInput } errorRight />
                <Field type='text' name="address.country" placeholder='Pays' component={ InsyMDBInput } errorRight />
            </div>
            <MDBBtn type='submit' disabled={isSubmitting} className="btn-block ses-background-2">Valider</MDBBtn>
            { errorForm && <ErrorMessSmall middle message="Cet email est déjà utilisé" /> }
        </Form>
    </Formik>
)


const VueProfil = ({salesman, handleClick}) => (
    <>
        <div>
            <p> Nom : <span> {salesman.firstName} </span> </p>
            <p> Prenom : <span> {salesman.lastName} </span> </p>
            <p> Email : <span> {salesman.email} </span> </p>
            <p> Telephone : <span> {salesman.phone} </span> </p>
        </div>
        <div>
            <p> Adresse : </p>
            {salesman &&
                <p>  
                    {salesman.address.number} {salesman.address.streetAddress}, {salesman.address.additional} {salesman.address.postalCode} {salesman.address.city} {salesman.address.country}  
                </p>
            }
        </div>
        <MDBBtn onClick={handleClick} className="ses-primary-background"> Modifier </MDBBtn>
    </>
) 


const ProfilAndUpdate = ({salesman, setSalesman}) => {


    const [update, setUpdate] = useState(false)
    const [errorForm, setErrorForm] = useState(false)

    const handleClick = () => {
        setUpdate(true);
    }

    const submitForm = (values) => {
        updateSalesman(values).then(response => {
            setSalesman(response.data)
            setUpdate(false)
        }).catch(() => setErrorForm(true))
    }

    const render = (
        update ?
            <>
                <FormProfil salesman={salesman} submitForm={submitForm} errorForm={errorForm} /> 
                <MDBBtn  className="btn-block ses-background-2" onClick={() => {setUpdate(false)}}>Retour</MDBBtn>
            </>
        : 
            <VueProfil salesman={salesman} handleClick={handleClick} />
    )

    return (

        <>
            {render}
        </>

    )
}
export default ProfilAndUpdate;

