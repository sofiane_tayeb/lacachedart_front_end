import { MDBBtn } from "mdbreact";
import ErrorMessSmall from "../../shared/components/form-and-error-components/ErrorMessSmall";
import { InsyMDBInput } from "../../shared/components/form-and-error-components/InputCustom";
import { Form, Formik, Field } from 'formik';
import { initialValuesNewPassword } from './../../shared/constants/formik-yup/default-values-form/idefaultValuesUser';
import { schemaFormNewPassword } from './../../shared/constants/formik-yup/yup/yupUser';
import { updatePassword } from "../../api/backend/account";
import { useState } from "react";
import { toast } from 'react-toastify';

const FormUpdatePassword = ({ submitForm, errorForm,  isSubmitting }) => (
    <Formik initialValues={initialValuesNewPassword} onSubmit={submitForm} validationSchema={schemaFormNewPassword}>
        <Form>
            <Field type="password" name="currentPassword" placeholder="Votre mot de passe actuel" component={ InsyMDBInput } errorRight />
            <Field type="password" name="newPassword" placeholder="Votre nouveau mot de passe" component={ InsyMDBInput } errorRight/>
            <Field type='password' name='confirmPassword' placeholder='Vérification mot de passe' component={ InsyMDBInput } errorRight />
            <MDBBtn type='submit' disabled={isSubmitting} className="btn-block ses-background-2">Valider</MDBBtn>
            { errorForm && <ErrorMessSmall middle message="Le mot de passe indiqué n'est pas valide" /> }
        </Form>
    </Formik>
)


const UpdatePassword = ({setUpdatePassword}) =>{

    const [errorForm, setErrorForm] = useState(false)

    const submitForm = (values) => {

        let passwordChange = {
            currentPassword : values.currentPassword,
            newPassword : values.newPassword
        }

        updatePassword(passwordChange).then(() =>{
            setUpdatePassword(false)
            toast.success('Votre Mot de passe a bien été modifié');
        }).catch(() => {
            toast.error('Votre mot de passe est invalide, veuillez recommencer');
            setErrorForm(true)
        })
    }

    return(
        <>
            <FormUpdatePassword submitForm={submitForm} errorForm={errorForm} />
        </>
    )
}

export default UpdatePassword;