import React, { useState } from 'react';
import { MDBInput } from 'mdbreact';
import { Formik, Form, Field } from 'formik';
import { InsyMDBInput } from '../../../shared/components/form-and-error-components/InputCustom';
import { schemaStoreCreate } from './../../../shared/constants/formik-yup/yup/yupUser';
import { initialValuesStoreCreate } from './../../../shared/constants/formik-yup/default-values-form/idefaultValuesStore';


const StoreCreateForm = () => {

    const [checkBox, setCheckBox] = useState(false);

    const StoreForm = ({ submit }) => (
        <Formik initialValues={initialValuesStoreCreate} onSubmit={submit} validationSchema={schemaStoreCreate} >
            <Form>
                <Field type="text" name="name" placeholder="Nom du magasin" component={InsyMDBInput} />
                <Field type='text' name='streetNumber' placeholder='Numéro de rue' component={InsyMDBInput} />
                <Field type='text' name='streetName' placeholder='Nom de rue' component={InsyMDBInput} />
                <Field type='text' name='complement' placeholder="Complément d'adresse" component={InsyMDBInput}/>
                <Field type='text' name='city' placeholder='Ville' component={InsyMDBInput} />
                <Field type='text' name='postalCode' placeholder='Code postal' component={InsyMDBInput}  />
                <Field type='text' name='url' placeholder="Si le magasin a un site web, veuillez l'indiquer" component={InsyMDBInput} />
            </Form>
        </Formik>
    )

    const toggleCheckbox = () => {
        setCheckBox(!checkBox);
    }

    return (
        <>
            <MDBInput
                label='Si vous avez acheté ce produit en magasin, cochez la case.'
                filled
                type='checkbox'
                id='checkbox1'
                containerClass='mr-5'
                onChange={toggleCheckbox}
            />

            {checkBox ?
                <StoreForm />
                : ""}
        </>

    );
};

export default StoreCreateForm;