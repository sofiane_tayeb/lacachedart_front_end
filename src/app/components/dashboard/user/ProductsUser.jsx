import { MDBCard, MDBCardBody, MDBCardFooter, MDBCardImage, MDBCardTitle, MDBCol, MDBContainer, MDBIcon, MDBRow } from 'mdbreact'
import React from 'react'
import DefaultImage from '../../../assets/images/banner-default-image.png'

const ProductsUser = (
    { userProductsRetrieved }) => {

    return (
        <MDBContainer className='mt-5'>

            <MDBRow>
                {
                    userProductsRetrieved.map(
                        (product, index) =>
                            <MDBCol md='4' key={index}>
                                <MDBCard wide className='mb-4'>
                                    <MDBCardImage
                                        cascade
                                        className='img-fluid'
                                        overlay="white-light"
                                        hover
                                        src={`${ product.photos ? product.photos : DefaultImage}`}
                                        alt={product.name}
                                    />

                                    <MDBCardBody cascade className='text-center'>
                                        <MDBCardTitle className='card-title'>
                                            <div>
                                                <strong className='mr-3'>{product.name}</strong>
                                                {product.active ?
                                                    <MDBIcon icon='check' className='green-text' />
                                                :
                                                    <MDBIcon icon='spinner' className='amber-text' />
                                                }
                                            </div>
                                        </MDBCardTitle>
                                        <p className='font-weight-bold blue-text'>{product.type}</p>
                                        <a href='#!' className='black-text d-flex justify-content-end'>
                                            <h5>
                                                En savoir plus
                                                <MDBIcon icon='angle-double-right' className='ml-2' />
                                            </h5>
                                        </a>
                                        <small className='small-font'>{product.creationDate}</small>
                                        <MDBCardFooter className='px-1'>
                                            
                                        </MDBCardFooter>
                                    </MDBCardBody>
                                </MDBCard>
                            </MDBCol>
                    )
                }
            </MDBRow>
        </MDBContainer>
    )
}

    export default ProductsUser