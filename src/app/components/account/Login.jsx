import React from 'react';
import { Formik, Form, Field } from 'formik';
import { MDBBtn } from 'mdbreact';
import { defaulValuesLogin } from './../../shared/constants/formik-yup/default-values-form/idefaultValuesUser';
import { schemaFormLogin } from './../../shared/constants/formik-yup/yup/yupUser';
import ErrorMessSmall from './../../shared/components/form-and-error-components/ErrorMessSmall';
import { InsyMDBInput } from '../../shared/components/form-and-error-components/InputCustom';

// test

const FormLogin = ({ submit, errorLog }) => (
    <Formik initialValues={defaulValuesLogin} onSubmit={submit} validationSchema={schemaFormLogin}>
        <Form>
            <Field type="text" name="username" placeholder="Login" component={InsyMDBInput} errorRight />
            <Field type='password' name='password' placeholder='Mot de passe' component={InsyMDBInput} errorRight />
            <MDBBtn type='submit' className="btn-block" color="primary">Se connecter</MDBBtn>
            {errorLog && <ErrorMessSmall middle message="Login/Mot de passe incorrect(s)" />}
        </Form>
    </Formik>
)

const Login = (props) => {
    return (
        <FormLogin {...props} />
    );
};

export default Login;