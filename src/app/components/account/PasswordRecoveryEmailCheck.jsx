import React from 'react';
import { Formik, Form, Field } from 'formik';
import { MDBCard, MDBCardBody, MDBCardTitle, MDBBtn } from 'mdbreact';
import { schemaFormEmailCheck } from '../../shared/constants/formik-yup/yup/yupUser';
import ErrorMessSmall from '../../shared/components/form-and-error-components/ErrorMessSmall';
import { InsyMDBInput } from '../../shared/components/form-and-error-components/InputCustom';
import { initialValuesEmailCheck } from './../../shared/constants/formik-yup/default-values-form/idefaultValuesUser';

/**
 * Component concerning the recovery of password
 * 
 * @returns the EmailCheckForPasswordRecovery component
 * 
 * @author Cecile
 */
const PasswordRecoveryEmailCheck = (props) => {

    const FormEmailCheck = ({ submit, errorLog }) => (
        <Formik initialValues={initialValuesEmailCheck} onSubmit={submit} validationSchema={schemaFormEmailCheck}>
            <Form>
                <Field type="text" name="mail" placeholder="Email" component={InsyMDBInput} errorRight />
                <MDBBtn type='submit' color="primary" className="btn-block">Valider</MDBBtn>
                {errorLog && <ErrorMessSmall middle message="Veuillez entrer un email." />}
            </Form>
        </Formik>
    )

    return (
        <div className="container d-flex justify-content-center mt-5">
            <MDBCard style={{ width: "22rem" }}>
                <MDBCardBody className="ses-background-2">
                    <MDBCardTitle className="text-center">Veuillez entrer votre email</MDBCardTitle>
                    <hr />
                    <FormEmailCheck {...props} />
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

export default PasswordRecoveryEmailCheck;