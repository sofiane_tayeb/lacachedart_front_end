import React, { useState } from 'react'
import useWindowDimensions from '../../shared/components/responsiveness/windowSizeGetter'
import * as yup from 'yup'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import { updateCurrentUser } from '../../api/backend/account'
import { MDBAlert, MDBBtn, MDBBtnGroup, MDBContainer, MDBJumbotron } from 'mdbreact'
import { toast } from 'react-toastify'

const Profile = ({currentProfile}) => {

    const { height, width } = useWindowDimensions()

    const [uniqueException, setUniqueException] = useState(false)

    const initialValues = {
        id: currentProfile.id,
        login: currentProfile.login,
        email: currentProfile.email,
        langKey: currentProfile.langKey,
        firstName: currentProfile.firstName,
        lastName: currentProfile.lastName
    }
    
    const validationSchema = yup.object().shape({
        email: yup.string().required().min(5, "L'adresse email doit contenir au moins 5 caractères").matches('^.+@.+(\\.[^\\.]+)+$', "Format d'email invalide"),
        firstName: yup.string().required().max(50, "Le prénom ne doit pas excéder 50 caractères").nullable(),
        lastName: yup.string().required().max(50, "Le nom ne doit pas excéder 50 caractères").nullable()
    })

    const submit = (values) => {
        updateCurrentUser(values)
        .then(res => {
            res.status === 200 && successRegistration() ; setUniqueException(false)
        })
        .catch((err, response) => {
            err.response.status === 400 && setUniqueException(true)
        })
    }

    const successRegistration = () => {
        toast.success('Profil modifié avec succès !', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            });
    }

    return(
        <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
        >
            {({ isSubmitting, resetForm, errors, touched }) => (
                <MDBContainer className="w-responsive text-center mx-auto p-5 mt-2">
                    <MDBJumbotron>
                        <div>
                            <h2>Modifier mon profil</h2>
                        </div>
                        <Form>
                            <div>
                                <Field type='hidden' name='id' />
                            </div>
                            <div>
                                <Field
                                    disabled='disabled'
                                    name='login'
                                    placeholder='Login'
                                    className={`form-control ${errors.login && touched.login && "border border-danger"}`}
                                />
                            </div>
                            <br></br>
                            <div>
                                <Field
                                    type='text'
                                    name='email'
                                    placeholder='Email'
                                    className={`form-control ${errors.email && touched.email && "border border-danger"}`}
                                />
                                <ErrorMessage 
                                    name='email' 
                                    component='small' 
                                    className='text-danger ml-2 float-right' 
                                />
                            </div>
                            <br></br>
                            <div>
                                <Field
                                    type='text'
                                    name='firstName'
                                    placeholder='Prénom'
                                    className={`form-control ${errors.firstName && touched.firstName && "border border-danger"}`}
                                />
                                <ErrorMessage 
                                    name='firstName' 
                                    component='small' 
                                    className='text-danger ml-2 float-right' 
                                />
                            </div>
                            <br></br>
                            <div>
                                <Field
                                    type='text'
                                    name='lastName'
                                    placeholder='Nom'
                                    className={`form-control ${errors.lastName && touched.lastName && "border border-danger"}`}
                                />
                                <ErrorMessage 
                                    name='lastName' 
                                    component='small' 
                                    className='text-danger ml-2 float-right' 
                                />
                            </div>
                            <br></br>
                            <div>
                                {uniqueException &&
                                    <MDBAlert color="danger">Utilisez un autre email</MDBAlert>
                                }
                            </div>
                            <MDBBtnGroup className={`${ width < 1000 && 'flex-column justify-content-center'}`}>
                                <MDBBtn 
                                    type='submit'
                                    disabled={isSubmitting} 
                                    className={`${ width >= 1000 ? 'mr-3 btn btn-primary btn-block' : 'btn btn-default'}`}>
                                    Modifier mon compte
                                </MDBBtn>
                                <MDBBtn 
                                    type='reset'
                                    onClick={() => { resetForm() ; setUniqueException(false) }} 
                                    className={`${ width >= 1000 ? 'mr-3 btn btn-primary btn-block' : 'btn btn-default'}`}>
                                    Reset
                                </MDBBtn>
                            </MDBBtnGroup>
                        </Form>
                    </MDBJumbotron>
                </MDBContainer>
            )} 

        </Formik>
    )
}

export default Profile