import React from 'react';
import { Formik, Form, Field } from 'formik';
import { MDBCard, MDBCardBody, MDBCardTitle, MDBBtn } from 'mdbreact';
import { schemaPasswordReset } from '../../shared/constants/formik-yup/yup/yupUser';
import ErrorMessSmall from '../../shared/components/form-and-error-components/ErrorMessSmall';
import { InsyMDBInput } from '../../shared/components/form-and-error-components/InputCustom';
import { initialValuesPasswordReset } from '../../shared/constants/formik-yup/default-values-form/idefaultValuesUser';

/**
 * Component concerning the password reset
 * 
 * @returns the PasswordReset component
 * 
 * @author Cecile
 */
const PasswordReset = (props) => {

    const FormPasswordReset = ({ submitForm, errorForm,  isSubmitting }) => (
        <Formik initialValues={initialValuesPasswordReset} onSubmit={submitForm} validationSchema={schemaPasswordReset}>
            <Form>
                <Field type="password" name="newPassword" placeholder="Nouveau mot de passe" component={ InsyMDBInput } errorRight />
                <Field type="password" name="newPasswordRepeat" placeholder="Répétez votre nouveau mot de passe" component={ InsyMDBInput } errorRight/>
                <MDBBtn type='submit' disabled={isSubmitting} color="primary" className="btn-block">Valider</MDBBtn>
                { errorForm && <ErrorMessSmall middle message="Erreur de réinitialisation du mot de passe" /> }
            </Form>
        </Formik>
    )

    return (
        <div className="container d-flex justify-content-center mt-5">
            <MDBCard style={{ width: "22rem" }}>
                <MDBCardBody className="ses-background-2">
                    <MDBCardTitle className="text-center">Veuillez entrer votre nouveau mot de passe</MDBCardTitle>
                    <hr />
                    <FormPasswordReset {...props} />
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

export default PasswordReset;