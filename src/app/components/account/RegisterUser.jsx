import React, { useState } from 'react'
import * as yup from 'yup'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import { registerUser } from '../../api/backend/account'
import { useHistory } from 'react-router-dom'
import { URL_LOGIN } from '../../shared/constants/urls/urlConstants'
import { MDBContainer, MDBJumbotron, MDBAlert, MDBBtnGroup, MDBBtn } from 'mdbreact'
import useWindowDimensions from '../../shared/components/responsiveness/windowSizeGetter'
import { toast } from 'react-toastify'

/**
 * @author jerome
 * 
 * Component used to register a new user. Switch to /login if response status is 201
 * 
 * @returns switch to /login
 */
const RegisterUser = () => {

    const history = useHistory()

    //gets dynamically window dimensions
    const { height, width } = useWindowDimensions()

    const initialValues = {
        login: '',
        password: '',
        email: ''
    }

    const successRegistration = () => {
        toast.success('Compte créé avec succès !', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            });
    }
    
    const validationSchema = yup.object().shape({
        login: yup.string().required('Login requis'),
        password: yup.string().required('Mot de passe requis').min(4, 'Le mot de passe doit contenir au moins 4 caractères'),
        email: yup.string().required('Email requis').min(5, "L'adresse email doit contenir au moins 5 caractères").matches('^.+@.+(\\.[^\\.]+)+$', "Format d'email invalide")
    })

    //If response status is 40_ -> switch to true in order to display an error through the modal
    const [uniqueLoginError, setUniqueLoginError] = useState(false)

    const [registerSuccess, setRegisterSuccess] = useState(false)

    const submit = (values) => {
        registerUser(values)
        .then(response => {
            response.status === 201 && successRegistration() ; switchToLogin()
        })
        .catch((err, response) => {
            err.response.status === 400 && setUniqueLoginError(true)
        })
    }

    const switchToLogin = () => {
        history.push(URL_LOGIN)
    }

    return(
        <Formik 
            initialValues={initialValues} 
            onSubmit={submit}
            validationSchema={validationSchema}
        >
            {({ isSubmitting, resetForm, errors, touched }) => (
                <MDBContainer className={`text-center mx-auto p-5 mt-2 ${ width < 1275 ? 'w-responsive mx-auto p-3 mt-2' : 'w-50' }`}>
                    <MDBJumbotron>
                        <div>
                            <h2>Créer un compte</h2>
                        </div>
                        <Form>
                            <div>
                                <Field
                                    type='text'
                                    name='email'
                                    placeholder='Email'
                                    className={`form-control ${errors.email && touched.email && "border border-danger"} ${ width > 1280 && 'w-responsive mx-auto p-3 mt-2' }`}
                                />
                                <ErrorMessage 
                                    name='email' 
                                    component='small' 
                                    className='text-danger ml-2 float-right' 
                                />
                            </div>
                            <br></br>
                            <div>
                                <Field
                                    type='text'
                                    name='login'
                                    placeholder='Login'
                                    className={`form-control ${errors.login && touched.login && "border border-danger"} ${ width > 1280 && 'w-responsive mx-auto p-3 mt-2' }`}
                                />
                                <ErrorMessage 
                                    name='login' 
                                    component='small' 
                                    className='text-danger ml-2 float-right' 
                                />
                            </div>
                            <br></br>
                            <div>
                                <Field
                                    type='password'
                                    name='password'
                                    placeholder='Password'
                                    className={`form-control ${errors.password && touched.password && "border border-danger"} ${ width > 1280 && 'w-responsive mx-auto p-3 mt-2' }`}
                                />
                                <ErrorMessage 
                                    name='password' 
                                    component='small' 
                                    className='text-danger ml-2 float-right' 
                                />
                            </div>
                            <br></br>
                            <div>
                                {uniqueLoginError &&
                                    <MDBAlert color="danger">Login ou email déjà utilisé</MDBAlert>
                                }
                            </div>
                            <MDBBtnGroup className={`${ width < 1280 && 'flex-column justify-content-center'}`}>
                                <MDBBtn 
                                    type='submit'
                                    disabled={isSubmitting} 
                                    className={`${ width >= 1280 ? 'mr-3 btn btn-primary btn-block' : 'btn btn-default'}`}>
                                    Créer mon compte
                                </MDBBtn>
                                <MDBBtn 
                                    type='reset'
                                    onClick={() => { resetForm() ; setUniqueLoginError(false) }} 
                                    className={`${ width >= 1280 ? 'mr-3 btn btn-primary btn-block' : 'btn btn-default'}`}>
                                    Reset
                                </MDBBtn>
                                <MDBBtn 
                                    onClick={() => switchToLogin()} 
                                    className={`${ width >= 1280 ? 'btn btn-primary btn-block' : 'btn btn-default'}`}>
                                    Déjà un compte ? Se connecter
                                </MDBBtn>
                            </MDBBtnGroup>
                        </Form>
                    </MDBJumbotron>
                </MDBContainer>
            )} 
        </Formik>
    )
}

export default RegisterUser