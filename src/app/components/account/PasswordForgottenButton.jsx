import React from 'react';
import { MDBBtn } from 'mdbreact';

/**
 * MDBBtn component concerning the recovery of password
 * 
 * @returns the PasswordForgottenButton component
 * 
 * @author Cecile
 */
const PasswordForgottenButton = ({ click }) => {
    return (
        <MDBBtn
            color="primary"
            onClick={click}
            className="btn-block">Mot de passe oublié
        </MDBBtn>
    )
};

export default PasswordForgottenButton;