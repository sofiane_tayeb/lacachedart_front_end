import React, { useState } from 'react';
import { URL_HOME, URL_CHOOSE_CATEGORY } from '../../shared/constants/urls/urlConstants';
import { MDBCollapse, MDBDropdown, MDBDropdownMenu, MDBDropdownToggle, MDBIcon, MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBNavItem, MDBNavLink } from 'mdbreact';
import ConnectionBtn from './ConnectionBtn';
import { useSelector } from 'react-redux';

const NavBar = () => {

    const [isOpen, setIsOpen] = useState(false)
    const isLogged = useSelector(({ authenticationReducer: { isLogged } }) => isLogged)

    const toggleCollapse = () => {
        setIsOpen(!isOpen)
    }

    return (
        <MDBNavbar color="special-color" dark expand="md">
            <MDBNavbarBrand>
                <strong className="white-text">La Cache d'Art</strong>
            </MDBNavbarBrand>
            <MDBNavbarToggler />
            <MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
                <MDBNavbarNav left>
                    <MDBNavItem>
                        <MDBNavLink to={URL_HOME}>Accueil</MDBNavLink>
                    </MDBNavItem>
                    {isLogged ?
                        <MDBNavItem>
                            <MDBNavLink to={URL_CHOOSE_CATEGORY}>Créer une fiche outil</MDBNavLink>
                        </MDBNavItem>
                    : ""}
                </MDBNavbarNav>
                <MDBNavbarNav right>
                    <MDBNavItem>
                        <MDBNavLink className="waves-effect waves-light" to="#!">
                            <MDBIcon fab icon="twitter" />
                        </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                        <MDBNavLink className="waves-effect waves-light" to="#!">
                            <MDBIcon fab icon="google-plus-g" />
                        </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                        <MDBDropdown>
                            <MDBDropdownToggle nav caret>
                                <MDBIcon icon="user" />
                            </MDBDropdownToggle>
                            <MDBDropdownMenu right>
                                <ConnectionBtn />
                            </MDBDropdownMenu>
                        </MDBDropdown>
                    </MDBNavItem>
                </MDBNavbarNav>
            </MDBCollapse>
        </MDBNavbar>
    )

};

export default NavBar;