import React from 'react';
import { MDBContainer, MDBNavLink } from 'mdbreact';
import { URL_HOME } from '../../shared/constants/urls/urlConstants';
import logo from './../../assets/images/lacachedart-logo.png';
import title from './../../assets/images/lacachedart-title.png';

const Header = () => {
    return (
        <MDBNavLink to={URL_HOME}>

            <MDBContainer className="d-flex
                align-items-center
                justify-content-center
                mt-4 mb-4 larger-container">

                <img className="logo" src={logo} alt="logo" />
                <img className="ml-3 mr-3" src={title} alt="title" />

            </MDBContainer>
            
        </MDBNavLink>
    )
}

export default Header;