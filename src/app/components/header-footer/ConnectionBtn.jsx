import React, { Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import { URL_LOGIN, URL_PROFILE, URL_REGISTER, URL_USER_DASHBOARD_PRODUCTS } from '../../shared/constants/urls/urlConstants';
import { useDispatch, useSelector } from 'react-redux';
import { URL_HOME } from '../../shared/constants/urls/urlConstants';
import { signOut } from '../../shared/redux-store/actions/authenticationActions';
import { MDBDropdownItem } from 'mdbreact';
import { toast } from 'react-toastify';


const ConnectionBtn = () => {

    const dispatch = useDispatch()
    const isLogged = useSelector(({ authenticationReducer: { isLogged } }) => isLogged)
    const history = useHistory();

    const handleSignOut = () => {
        toast.info("Vous êtes maintenant déconnecté. À bientôt !")
        dispatch(signOut())
        history.push(URL_HOME)
    }

    const LoginLink = () => <MDBDropdownItem href={URL_LOGIN} >Se connecter</MDBDropdownItem>

    const RegisterLink = () => <MDBDropdownItem href={URL_REGISTER} >S'enregistrer</MDBDropdownItem>

    const ProfileUpdate = () => <MDBDropdownItem href={URL_PROFILE} >Profil</MDBDropdownItem>

    const DeconnectionButton = () => <MDBDropdownItem onClick={handleSignOut} >Déconnexion</MDBDropdownItem>

    const UserProducts = () => <MDBDropdownItem href={URL_USER_DASHBOARD_PRODUCTS} >Mes produits</MDBDropdownItem>

    return isLogged ? 
        <Fragment>
            <div>    
                <ProfileUpdate />
                <UserProducts /> 
                <DeconnectionButton />    
            </div>
        </Fragment>
    :
        <Fragment>
            <div>    
                <LoginLink />
                <RegisterLink />     
            </div>
        </Fragment>
}

export default ConnectionBtn;
