import { MDBCard, MDBCardBody, MDBCardText, MDBCardTitle, MDBIcon } from 'mdbreact';

/**
 * Ligne représentant une compagnie. Implémentée dans Search.
 *
 * @param {object} companie : contient l'objet compagnie a afficher 
 * @param {function} goCieView : Cette méthode est implémentée dans SearchCieView, permet de se déplacer dans le tableau de bord de la compagnie ciblée
 * 
 * @author Juliette Yguel
 */
const CieRow = ({companie, goCieView, api}) => {

    return (
        <MDBCard className="ses-background-blue mb-2">
            <MDBCardBody className="d-flex justify-content-between flex-row align-items-center mb-2 ses-white " onClick={() => goCieView(companie, api)} style={{cursor: "pointer"}}>
                <MDBCardTitle className="mb-0 align-self-center ses-white col-6">{companie.companyName} {companie.rs}</MDBCardTitle>
                <MDBCardText className="mb-0  align-self-center col"> Siren : {companie.siren}</MDBCardText>
                <MDBIcon far icon="eye" onClick={() => goCieView(companie.id, api)}  className="text-center col" size="2x"/>
            </MDBCardBody>
        </MDBCard>
    )
}
export default CieRow