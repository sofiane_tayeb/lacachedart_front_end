import React, { useState } from "react";
import CieRow from "./CieRow";
import SearchCie from "./SearchCie";
import { searchCie } from "../../api/backend/company";
import ImageCompany from '../../assets/images/office-building.svg'
import { MDBContainer } from "mdbreact";
import Loader from '../../shared/components/utils-components/Loader';
import { searchCieApi } from "../../api/backend/external-api";


/**
 * Composant permettant de rechercher et d'afficher des card de compagnies trouvées grace a la recherche. Implémenté dans Search.
 *
 * @param {function} goCieView : Cette méthode est implémentée dans SearchCieView, permet de se déplacer dans le tableau de bord de la compagnie ciblée. Elle est passée en props a RowCie
 * 
 * @author Juliette Yguel
 */
const Search = ({goCieView}) => {

    const [search, setSearch] = useState("");
    const [companies, setCompanies] = useState([]);
    const [click, setClick] = useState(false);
    const [loading, setLoading] = useState(false);
    const [externalApi, setExternalApi] = useState(false)
  
    const onChange = (e) => {setSearch(e.target.value);};
  
    const handleSearch = async () => {
      if (search !== "") {
        setLoading(true);
        setCompanies([]);
       	let response =  await searchInternalApi();
        checkInternalApiContainsRow(response);
        setSearch("");
      }
    };

   const searchInternalApi = async () => {
	   let cie
    	await searchCie(search).then(response => {
			console.log(1);
			setCompanies(response.data);
			cie = response.data
		}, error => {
			console.log(error);
			setLoading(false);
		})
		return cie
    }

	const checkInternalApiContainsRow = (response) => {
		if(response.length > 0){
			console.log("api interne ok");
			setLoading(false);
			setClick(true);
			setExternalApi(false)
		}else{
			console.log("api interne pas ok");
			setExternalApi(true)
			searchExternalApi();
		}
	}

	const searchExternalApi =  () => {
		searchCieApi(search).then(response =>{
			console.log("search external API");
            setCompanies(response.data)
            setLoading(false);
            setClick(true);
          }, error => {
            console.log(error)
            setLoading(false);
            setClick(true);
          })
	}


    // Affichage conditionel permettant l'affichage des résultats ou l'indication qu'aucune cie n'a été trouvée
    const viewCie = (
     click && companies.length > 0 && loading===false
     ?  companies.map((cie, index) => (<CieRow  companie={cie} api={externalApi} goCieView={goCieView} key={index}/>))
     : click && companies.length === 0 && loading===false
     ? <div className="text-center ses-error"> Aucun résultat trouvé </div>
     : null 
    )
  
  
    return (
      <MDBContainer className="d-flex justify-content-center flex-column ">
          <div className="text-center mt-5">
              <img src={ImageCompany} alt="company" className="search-cie-img"/>
              <h1 className=".ses-text-primary mt-2">Recherche Entreprise</h1>
          </div>
          <SearchCie handleSearch={handleSearch} onChange={onChange} search={search} />
          {loading && <Loader/>}
          {viewCie}
      </MDBContainer>
    );
}
export default Search