import { MDBInput, MDBBtn } from "mdbreact";

/**
 * 
 * @param
 * 
 * @author Juliette Yguel
 */
const SearchCie = ({ handleSearch, onChange, search }) => {
  return (
    <div className="d-flex justify-content-md-center flex-row">
      <MDBInput label="Entrez le nom ou le siren de l'entreprise recherchée" icon="search" onChange={onChange} value={search} className="mr-5 col align-self-center"/>
      <MDBBtn rounded onClick={handleSearch} className="btn-lg ses-background-blue3 align-self-center ml-5">
        Valider
      </MDBBtn>
    </div>
  );
};

export default SearchCie;
