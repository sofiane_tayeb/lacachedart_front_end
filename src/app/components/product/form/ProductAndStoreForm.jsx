import React, { useState, useEffect, } from 'react';
import { Form, Formik } from 'formik';
import { useLocation } from 'react-router';
import StoreCreateForm from '../../store/form/StoreCreateForm';
import ProductCreateForm from './ProductCreateForm';
import { URL_CHOOSE_CATEGORY } from './../../../shared/constants/urls/urlConstants';
import { MDBContainer, MDBCol, MDBRow, MDBBtn } from 'mdbreact';
import { initialValuesProductAndStoreCreate } from './../../../shared/constants/formik-yup/default-values-form/idefaultValuesProductAndStore';

const ProductAndStoreForm = ({ history }) => {

    const location = useLocation();
    const mainCategoryId = location.state ? location.state.mainCategoryId : -1

    const ProductForm = ({ submit, errorLog }) => (

        <Formik initialValues={initialValuesProductAndStoreCreate} onSubmit={submit} validationSchema={schemaProductAndStoreCreate} >
            <Form>
                <Field type="text" name="name" placeholder="Nom de l'outil" component={InsyMDBInput} errorRight />

                <Field
                    type="select"
                    name="brand"
                    options={optionBrands}
                    component={InsyMDBSelect}
                    label="Marque"
                />

                <Field
                    type="select"
                    name="subcategory"
                    options={optionSubcategories}
                    component={InsyMDBSelect}
                    label="Technique"
                />

                <Field type='textarea' name='description' placeholder='Description (400 caractères maximum)' component={InsyMDBInput} errorRight />
                {errorLog && <ErrorMessSmall middle message="Erreur, veuillez compléter les champs requis" />}
            </Form>
        </Formik>
    )

    useEffect(() => {

        if (mainCategoryId !== -1) {

            getAllBrands().then(response => {
                setBrands(response.data)

            }).catch(error => {
                console.log(error)
            })

            getSubcategoriesByMainCategoryId(mainCategoryId).then(response => {
                setSubcategories(response.data);
            }).catch(error => {
                console.log(error)
            })

            setLoading(false);

        } else {
            history.push(URL_CHOOSE_CATEGORY);
        }
    }, [])


    const optionBrands = brands.map(brnd => {
        let brand = {
            text: brnd.name,
            value: brnd.id
        }
        return brand
    })

    const optionSubcategories = subcategories.map(subcateg => {
        let subcategory = {
            text: subcateg.name,
            value: subcateg.id
        }
        return subcategory
    })


    return (

            <MDBContainer className="d-flex justify-content-center flex-column mt-5 larger-container">
                <MDBRow>

                    <MDBCol className='col-6'>
                        <h5>Caractéristiques de l'outil </h5>

                        <ProductCreateForm mainCategoryId={mainCategoryId} />
                    </MDBCol>

                    <MDBCol className='col-6'>
                        <StoreCreateForm />
                    </MDBCol>

                    <MDBBtn type='submit' className="btn-block" color="primary" onClick={handleSubmit}>Créer la fiche outil</MDBBtn>

                </MDBRow>
            </MDBContainer>
    );
};

export default ProductAndStoreForm;