import React, { useState, useEffect, } from 'react';
import { useLocation } from 'react-router';
import { toast } from 'react-toastify';
import ProductCreateForm from './ProductCreateForm';
import { URL_CHOOSE_CATEGORY, URL_HOME } from './../../../shared/constants/urls/urlConstants';
import { MDBCard, MDBCardBody } from 'mdbreact';
import { createProduct } from './../../../api/backend/product';

const ProductCreate = ({ history }) => {


    const location = useLocation();
    const mainCategoryId = location.state ? location.state.mainCategoryId : -1
    const [loading, setLoading] = useState(true);


    useEffect(() => {

        if (mainCategoryId === -1) {
            history.push(URL_CHOOSE_CATEGORY);
        } else {
            setLoading(false);
        }
    }, [mainCategoryId, history])

    const handleSubmit = (values) => {
        createProduct(values).then(response => {
            if (response.status === 201) {
                toast.success("Fiche outil créée !")
                history.push(URL_HOME)
            }
        }).catch((error) => console.log(error))
    };


    return (
        loading ? "" :

            <div className="container d-flex justify-content-center mt-5">

                <MDBCard style={{ width: "40rem" }}>
                    <MDBCardBody className="ses-background-2">

                        <h5>Caractéristiques de l'outil </h5>
                        <ProductCreateForm mainCategoryId={mainCategoryId} submit={handleSubmit} />

                    </MDBCardBody>
                </MDBCard>

            </div>
    );
};

export default ProductCreate;