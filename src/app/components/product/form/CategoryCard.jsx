import React from 'react';
import { MDBBtn, MDBCard, MDBCardTitle, MDBCardBody, MDBCardGroup, MDBCardImage, MDBCol } from 'mdbreact';
import { useHistory } from 'react-router-dom';
import { URL_CREATE_PRODUCT } from './../../../shared/constants/urls/urlConstants';
import painting from './../../../assets/images/paint-tool.png';
import drawing from './../../../assets/images/drawing-tool.png';
import engraving from './../../../assets/images/engraving-tool.png';
import sculpture from './../../../assets/images/sculpture-tool.png';
import unknown from './../../../assets/images/unknown.png';


const CategoryCard = ({ category, card }) => {

    const history = useHistory();

    const addImage = (name) => {
        switch (name) {
            case 'Peinture':
                return painting;
            case 'Dessin':
                return drawing;
            case 'Gravure':
                return engraving;
            case 'Sculpture':
                return sculpture;
            default:
                return unknown;
        }
    };

    return (
        <MDBCol className='col-5 '>
            {
                <MDBCardGroup>

                    <MDBCard narrow >
                        <MDBCardImage src={ addImage(category.name)}/>
                            <MDBCardTitle background={ addImage(category.name) } className = 'd-flex justify-content-center mt-2'>
                                {category.name}
                            </MDBCardTitle>
                            <h2 className='h2-responsive'>{card.cardName}</h2>
                        <MDBCardBody className='d-flex justify-content-center'>
                            <MDBBtn
                                color='primary'
                                onClick={() => {
                                    history.push({ pathname: URL_CREATE_PRODUCT, state: { mainCategoryId: category.id } })
                                }}
                                className='btn-sm '
                            >
                                Choisir cette pratique
                                </MDBBtn>
                        </MDBCardBody>
                    </MDBCard>

                </MDBCardGroup>
            }

        </MDBCol>
    )
}

export default CategoryCard;