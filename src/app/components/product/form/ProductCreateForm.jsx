import React, { useState, useEffect, } from 'react';
import { useLocation } from 'react-router';
import { MDBBtn } from 'mdbreact';
import { Formik, Form, Field } from 'formik';
import { getAllBrands } from '../../../api/backend/brand';
import { initialValuesProductCreate } from '../../../shared/constants/formik-yup/default-values-form/idefaultValuesProduct';
import { schemaProductCreate } from '../../../shared/constants/formik-yup/yup/yupUser';
import { InsyMDBInput, InsyMDBSelect } from '../../../shared/components/form-and-error-components/InputCustom';
import ErrorMessSmall from '../../../shared/components/form-and-error-components/ErrorMessSmall';
import { getSubcategoriesByMainCategoryId } from '../../../api/backend/categories';
import { getToolTypesByCategoryId } from './../../../api/backend/toolType';


const ProductForm = ({ submit, errorLog, optionBrands, optionSubcategories, optionToolTypes }) => (

    <Formik initialValues={initialValuesProductCreate} onSubmit={submit} validationSchema={schemaProductCreate} >
        <Form>
            <Field type="text" name="name" label="Nom de l'outil" component={InsyMDBInput} errorRight />

            <Field
                type="select"
                name="brandId"
                options={optionBrands}
                component={InsyMDBSelect}
                label="Marque"
            />

            <Field
                type="select"
                name="categoryId"
                options={optionSubcategories}
                component={InsyMDBSelect}
                label="Technique"
            />

            <Field
                type="select"
                name="toolTypeId"
                options={optionToolTypes}
                component={InsyMDBSelect}
                label="Type d'outil"
            />

            <>Si vous connaissez le prix fabricant, indiquez-le :  </>
            <Field type='number' name='manufacturerPrice' label='Si vous connaissez le prix fabricant, indiquez-le (euros)' /> €
            <Field type='textarea' name='description' label='Description (400 caractères maximum)' maxlength='400' rows="6" cols="30" component={InsyMDBInput} errorRight />
            <MDBBtn type='submit' className="btn-block" color="primary">Créer la fiche outil</MDBBtn>
            {errorLog && <ErrorMessSmall middle message="Erreur, veuillez compléter les champs requis" />}
        </Form>
    </Formik>
)


const ProductCreateForm = (props) => {

    const location = useLocation();
    const mainCategoryId = location.state ? location.state.mainCategoryId : -1
    const [brands, setBrands] = useState([]);
    const [subcategories, setSubcategories] = useState([]);
    const [toolTypes, setToolTypes] = useState([]);
    const [loading, setLoading] = useState(true);


    useEffect(() => {

        getAllBrands().then(response => {
            setBrands(response.data)
        }).catch(error => {
            console.log(error)
        })

        getSubcategoriesByMainCategoryId(mainCategoryId).then(response => {
            setSubcategories(response.data);
        }).catch(error => {
            console.log(error)
        })

        getToolTypesByCategoryId(mainCategoryId).then(response => {
            setToolTypes(response.data);
        }).catch(error => {
            console.log(error)
        })

        setLoading(false);
    }, [mainCategoryId])


    const optionBrands = brands.map(brnd => {
        let brand = {
            text: brnd.name,
            value: brnd.id
        }
        return brand
    })

    const optionSubcategories = subcategories.map(subcateg => {
        let subcategory = {
            text: subcateg.name,
            value: subcateg.id
        }
        return subcategory
    })

    const optionToolTypes = toolTypes.map(toolTy => {
        let toolType = {
            text: toolTy.name,
            value: toolTy.id
        }
        return toolType
    })


    return (
        loading ? "" :
            <ProductForm {...props} optionBrands={optionBrands} optionSubcategories={optionSubcategories} optionToolTypes={optionToolTypes}/>
    );
};

export default ProductCreateForm;