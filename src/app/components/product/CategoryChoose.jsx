import React, { useState, useEffect } from 'react';
import CategoryCard from './form/CategoryCard';
import { getAllMainCategories } from '../../api/backend/categories';
import { MDBContainer, MDBRow} from 'mdbreact';

const CategoryChoose = () => {

    const [loading, setLoading] = useState(false);
    const [mainCategories, setMainCategories] = useState([]);

    useEffect(() => {

        setLoading(true);

        getAllMainCategories().then(response => {
            setMainCategories(response.data)

        }).catch(error => {
            console.log(error)
        })

        setLoading(false);
    }, [])

    const cards = [
        {

        }
    ];

    return (

        loading ? ""
            :
            <>
                <h3 className="d-flex justify-content-center mt-5">À quelle pratique artistique appartient l'outil ?</h3>

                <MDBContainer className="d-flex justify-content-center flex-column mt-2">
                    <MDBRow className="d-flex justify-content-center mt-2">
                        {cards.map(card => mainCategories.map((category, id) =>
                            <CategoryCard category={category} card={card} key={id} />
                        ))}
                    </MDBRow>
                </MDBContainer>
            </>

    );
};

export default CategoryChoose;