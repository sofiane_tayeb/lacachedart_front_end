import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import NavBar from './../components/header-footer/NavBar';
import Routes from './Routes';
import IdleTimerCustom from '../components/account/IdleTimerCustom';
import { useSelector } from 'react-redux';
import Header from './../components/header-footer/Header';


const RoutesWithNavigation = () => {
    
    const isLogged = useSelector(({authenticationReducer:{ isLogged }}) => isLogged)

    return (
        <BrowserRouter>
            { isLogged && <IdleTimerCustom />}
            <Header/>
            <NavBar/>
            <main>
                <Routes/>
            </main>
            <ToastContainer position="bottom-right" />
        </BrowserRouter>
    );
};

export default RoutesWithNavigation;