import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { URL_LOGIN, URL_EMAIL_CHECK, URL_PASSWORD_RESET, URL_PASSWORD_RESET_NO_KEY, URL_REGISTER, URL_CHOOSE_CATEGORY, URL_CREATE_PRODUCT, URL_PROFILE, URL_USER_DASHBOARD_PRODUCTS } from "./../shared/constants/urls/urlConstants";
import { customHistory } from "./../shared/services/historyServices";
import { ROLE_ADMIN, ROLE_USER, ROLE_CONTRIBUTOR, ROLE_SUPERCONTRIBUTOR, ROLE_MODERATOR } from '../shared/constants/rolesConstant'
import { PrivateRoute } from "../shared/components/utils-components/PrivateRoute";
//import HomeView from "../views/HomeView";
import LoginView from "../views/LoginView";
//import AdminHomeView from "./../views/AdminHomeView";
import PasswordRecoveryEmailCheckView from '../views/account/PasswordRecoveryEmailCheckView';
import PasswordResetView from './../views/account/PasswordResetView';
import RegisterUser from "../components/account/RegisterUser";
import ProfileView from "../views/account/ProfileView";
import ProductsUserView from '../views/dashboard/user/ProductsUserView'
import ProductCreateView from './../views/product/ProductCreateView';
import ProductCreate from './../components/product/form/ProductCreate';


const Routes = () => {

	return (
		<Switch history={customHistory}>
			<Route path={URL_LOGIN} component={LoginView} />

			{/* Account  */}
			<Route path={URL_REGISTER} component={RegisterUser} />
			<PrivateRoute path={URL_PROFILE} component={ProfileView} />

			{/* Product  */}
			<PrivateRoute path={URL_USER_DASHBOARD_PRODUCTS} component={ProductsUserView} roles={[ROLE_ADMIN, ROLE_USER, ROLE_CONTRIBUTOR, ROLE_SUPERCONTRIBUTOR, ROLE_MODERATOR]} />
			<PrivateRoute path={URL_CHOOSE_CATEGORY} component={ProductCreateView} />
			<PrivateRoute path={URL_CREATE_PRODUCT} component={ProductCreate} />
			
			<Route exact path={URL_EMAIL_CHECK} component={PasswordRecoveryEmailCheckView} />
			<Route from={URL_PASSWORD_RESET_NO_KEY} exact>
				<Redirect to={URL_LOGIN}/> {/* Automatic redirection if the user tries this route without the key parameter */}
			</Route>
			<Route path={URL_PASSWORD_RESET} component={PasswordResetView} />
			
		</Switch>
  );
};

export default Routes;
