import React from 'react'
import { Provider } from 'react-redux';
import RoutesWithNavigation from './routes/RoutesWithNavigation'
import configureStore from './shared/redux-store/store';

//store redux
const store = configureStore()


const App = () => {
	return (
		<Provider store={ store }>
			<RoutesWithNavigation/>
		</Provider>
	);
};

export default App;